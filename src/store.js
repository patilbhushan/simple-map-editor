import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state () {
    return {
      isDrawing: false,
      selectedIds: {},
      features: []
    }
  },
  mutations: {
    addFeature (state, feature) {
      state.features.push(Object.assign({}, feature))
      if (feature.id) state.selectedIds = { ...state.selectedIds, [feature.id]: false }
    },
    clearFeatures (state) {
      state.selectedIds = {}
      state.features = []
    },
    setIsDrawing (state, isDrawing) {
      state.isDrawing = isDrawing
    },
    setSelectedIds (state, ids) {
      const newIds = {}
      Object.keys(state.selectedIds).forEach(id => newIds[id] = false)
      if (Array.isArray(ids)) ids.forEach(id => newIds[id] = true)
      state.selectedIds = newIds
    },
    toggleSelectedId (state, id) {
      state.selectedIds[id] = !state.selectedIds[id]
    },
  }
})

export default store